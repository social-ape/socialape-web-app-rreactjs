import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

// Redux 
import { connect } from 'react-redux';
import { getUserData } from '../redux/actions/dataActions';

// Mui
import Grid from '@material-ui/core/Grid';

// Components
import Scream from '../components/scream/Scream';
import StaticProfile from '../components/profile/StaticProfile';
import ProfileSkeleton from '../utils/ProfileSkeleton';
import ScreamSkeleton from '../utils/ScreamSkeleton';

class User extends Component {

    state = {
        profile: null,
        screamIdParam: null
    }

    componentDidMount() {

        const { handle, screamId } = this.props.match.params;

        if (screamId) this.setState({ screamIdParam: screamId });

        this.props.getUserData(handle);
        axios.get(`user/${handle}`)
            .then(res => this.setState({ profile: res.data.user }))
            .catch(err => console.log('componentDidMount', { err }));
    }

    render() {

        const { screams, loading } = this.props.data;
        const screamIdParam = this.state.screamIdParam

        const sreamsMarup = loading
            ? <ScreamSkeleton />
            : screams === null
                ? (<p>This user is scilent till now </p>)
                : !screamIdParam
                    ? (screams.map(scream => <Scream key={scream.screamId} scream={scream} />))
                    : (
                        screams.map(scream =>
                            scream.screamId !== screamIdParam
                                ? <Scream key={scream.screamId} scream={scream} />
                                : <Scream key={scream.screamId} scream={scream} openDialog />
                        )
                    )

        return (
            <Grid container spacing={2}>
                <Grid item sm={8} xs={12}>
                    {sreamsMarup}
                </Grid>
                <Grid item sm={4} xs={12}>
                    {
                        this.state.profile === null
                            ? <ProfileSkeleton />
                            : <StaticProfile profile={this.state.profile} />
                    }
                </Grid>
            </Grid>
        )
    }
}

User.propTypes = {
    getUserData: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    data: state.data
});

export default connect(mapStateToProps, { getUserData })(User);
