import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

// Components

// Redux
import { connect } from 'react-redux';
import { markNotificationRead } from '../../redux/actions/userActions';

// MUI stuff
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import ToolTip from '@material-ui/core/ToolTip';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';

// Icons
import NotificationIcon from '@material-ui/icons/Notifications';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ChatIcon from '@material-ui/icons/Chat';

class Notifications extends Component {

    state = {
        anchorEl: null
    };

    handleOpen = event => {
        this.setState({ anchorEl: event.target });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    onMenuOpened = () => {
        let unreadNotificationsIds = this.props.notifications
            .filter(not => !not.read)
            .map(not => not.notificationId)
        this.props.markNotificationRead(unreadNotificationsIds);
    };

    render() {

        const notifications = this.props.notifications;
        const anchorEl = this.state.anchorEl;

        dayjs.extend(relativeTime);

        let notificationIcon;
        if (notifications && notifications.length > 0) {
            const notificationCount = notifications.filter(not => not.read === false).length;
            notificationIcon = notificationCount > 0
                ? (
                    <Badge badgeContent={notificationCount} color="secondary" >
                        <NotificationIcon />
                    </Badge>
                ) : (<NotificationIcon />)
        } else {
            notificationIcon = <NotificationIcon />
        }

        let notificationsMarkup =
            notifications && notifications.length > 0
                ? (
                    notifications.map(not => {
                        const { type, createdAt, read, recipient, screamId, sender } = not;
                        const verb = type === 'like' ? 'liked' : 'commented on';
                        const time = dayjs(createdAt).fromNow();
                        const iconColor = read ? 'primary' : 'secondary';
                        const icon = type === 'like'
                            ? <FavoriteIcon color={iconColor} style={{ marginRight: 10 }} />
                            : <ChatIcon color={iconColor} style={{ marginRight: 10 }} />

                        return (
                            <MenuItem key={createdAt} onClick={this.handleClose}>
                                {icon}
                                <Typography
                                    component={Link}
                                    color="primary"
                                    variant="body1"
                                    to={`/users/${recipient}/scream/${screamId}`}>
                                    {sender} {verb} your scream {time}.
                                </Typography>
                            </MenuItem>
                        )
                    })
                ) : <MenuItem onClick={this.handleClose}> You have no Notifications </MenuItem>

        return (
            <>
                <ToolTip placement="top" title="Notifications">
                    <IconButton aria-owns={anchorEl ? 'simple-menu' : undefined}
                        aria-haspopup="true"
                        onClick={this.handleOpen}>
                        {notificationIcon}
                    </IconButton>
                </ToolTip>
                <Menu anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                    onEntered={this.onMenuOpened}>
                    {notificationsMarkup}
                </Menu>
            </>
        )
    }
}

Notifications.propTypes = {
    markNotificationRead: PropTypes.func.isRequired,
    notifications: PropTypes.array.isRequired
};

const mapStateToProps = state => ({ notifications: state.user.notifications });

export default connect(mapStateToProps, { markNotificationRead })(Notifications);
