import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';

// Components
import MyButton from '../../utils/MyButton';
import LikedButton from './LikedButton';
import Comments from './Comments';
import CommentForm from './CommentForm';

// Redux
import { connect } from 'react-redux';
import { getScream, clearErrors } from '../../redux/actions/dataActions';

// MUI stuff
import withStyles from '@material-ui/core/styles/withStyles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

// Icons
import UnfoldMore from '@material-ui/icons/UnfoldMore';
import CloseIcon from '@material-ui/icons/Close';
import ChatIcon from '@material-ui/icons/Chat';

import formStyles from '../../utils/formStyles';
import theme from '../../utils/theme';

const styles = {
    ...formStyles,
    ...theme,
    invisibleSeparator: {
        border: 'none',
        margin: 4
    },
    visibleSeparator: {
        width: '100%',
        borderBottom: '1px solid rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    profileImage: {
        maxWidth: 200,
        height: 200,
        objectFit: 'cover',
        borderRadius: '50%'
    },
    dialogContent: {
        padding: 20
    },
    closeButton: {
        position: 'absolute',
        left: '90%'
    },
    expandButton: {
        position: 'absolute',
        left: '90%'
    },
    spinnerDiv: {
        textAlign: 'center',
        marginTop: 50,
        marginBottom: 50
    }
};

class ScreamDialog extends Component {

    state = {
        open: false,
        oldPath: null,
        newPath: null
    };

    componentDidMount() {
        if (this.props.openDialog) {
            this.handleOpen();
        }
    }

    handleOpen = () => {

        let oldPath = window.location.pathname;

        const { userHandle, screamId } = this.props;
        const newPath = `/users/${userHandle}/scream/${screamId}`;

        if (oldPath === newPath) oldPath = `/users/${userHandle}`;

        window.history.pushState(null, null, newPath);

        this.setState({ open: true, oldPath, newPath });
        this.props.getScream(this.props.screamId);
    }

    handleClose = () => {

        window.history.pushState(null, null, this.state.oldPath);

        this.setState({ open: false });
        this.props.clearErrors();
    }

    render() {

        const {
            classes,
            scream: { scream },
            UI: { loading }
        } = this.props;

        const dialogMarkup = loading
            ? (
                <div className={classes.spinnerDiv} >
                    <CircularProgress size={100} thickness={2} />
                </div>
            )
            : (
                scream &&
                <Grid container spacing={10}>
                    <Grid item sm={5}>
                        <img src={scream.userImage} alt="Profile" className={classes.profileImage} />
                    </Grid>
                    <Grid item sm={7}>
                        <Typography
                            variant="h5"
                            component={Link}
                            to={`/users/${scream.userHandle}`}
                            color="primary">
                            @{scream.userHandle}
                        </Typography>
                        <hr className={classes.invisibleSeparator} />
                        <Typography
                            variant="body2"
                            color="textSecondary">
                            {dayjs(scream.createdAt).format('h:mm: a, MMMM DD YYYY')}
                        </Typography>
                        <hr className={classes.invisibleSeparator} />
                        <Typography variant="body1">
                            {scream.body}
                        </Typography>
                        <LikedButton screamId={scream.id || scream.screamId} />
                        <span>{scream.likeCount} Likes</span>
                        <MyButton tip="Comments">
                            <ChatIcon color="primary" />
                        </MyButton>
                        <span>{scream.commentCount} Comments</span>
                    </Grid>
                    <hr className={classes.visibleSeparator} />
                    <CommentForm screamId={scream.screamId || scream.id} />
                    <Comments comments={scream.comments} />
                </Grid>
            );

        return (
            <>
                <MyButton
                    tip="Expand Scream"
                    onClick={this.handleOpen}
                    tipClassName={classes.expandButton}>
                    <UnfoldMore color="primary" />
                </MyButton>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    fullWidth
                    maxWidth="sm">
                    <MyButton
                        tip="Close"
                        onClick={this.handleClose}
                        tipClassName={classes.closeButton}>
                        <CloseIcon />
                    </MyButton>
                    <DialogContent className={classes.dialogContent}>
                        {dialogMarkup}
                    </DialogContent>
                </Dialog>
            </>
        )
    }
}

ScreamDialog.propTypes = {
    getScream: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired,
    screamId: PropTypes.string.isRequired,
    userHandle: PropTypes.string.isRequired,
    scream: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    scream: state.data,
    UI: state.UI
});

export default connect(mapStateToProps, { getScream, clearErrors })(withStyles(styles)(ScreamDialog));
