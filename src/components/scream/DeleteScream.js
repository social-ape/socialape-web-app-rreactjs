import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import MyButton from '../../utils/MyButton';

// Redux
import { connect } from 'react-redux';
import { deleteScream } from '../../redux/actions/dataActions';

// MUI stuff
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

//Icons
import DeleteOutline from '@material-ui/icons/DeleteOutline';

const stysle = {
    delete: {
        position: 'absolute',
        top: '10px',
        left: '90%'
    }
};

class DeleteScream extends Component {

    state = { open: false };

    handleOpen = () => this.setState({ open: true });

    handleClose = () => this.setState({ open: false });

    handleDeleteScream = () => {
        this.props.deleteScream(this.props.screamId);
        this.setState({ open: false });
    }

    render() {
        const { classes } = this.props;
        return (
            <>
                <MyButton
                    tip="Delete Scream"
                    btnClassName={classes.delete}
                    onClick={this.handleOpen}>
                    <DeleteOutline color="secondary" />
                </MyButton>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    fullWidth
                    maxWidth="sm">
                    <DialogTitle>Are you sure you want to delete this scream ?</DialogTitle>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">Cancel</Button>
                        <Button onClick={this.handleDeleteScream} color="secondary">Delete</Button>
                    </DialogActions>
                </Dialog>
            </>
        )
    }
}

DeleteScream.propTypes = {
    deleteScream: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    screamId: PropTypes.string.isRequired
};

export default connect(null, { deleteScream })(withStyles(stysle)(DeleteScream));
