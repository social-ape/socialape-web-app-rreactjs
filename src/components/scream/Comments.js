import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';

// MUI stuff
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import formStyles from '../../utils/formStyles';
import theme from '../../utils/theme';

const styles = {
    ...formStyles,
    ...theme,
    invisibleSeparator: {
        border: 'none',
        margin: 4
    },
    visibleSeparator: {
        width: '100%',
        borderBottom: '1px solid rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    commentImage: {
        maxWidth: '100%',
        height: 100,
        width: 100,
        objectfit: 'cover',
        borderRadius: '50%'
    },
    commentData: {
        marginLeft: 20
    }
};

class Comments extends Component {
    render() {

        const { classes, comments } = this.props;

        return (
            <Grid container style={{ marginLeft: 50 }}>
                {
                    comments.map((comment, index) => {
                        const { body, createdAt, userImage, userHandle } = comment;
                        return (
                            <Fragment key={createdAt}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={2}>
                                            <img src={userImage} alt="comment" className={classes.commentImage} />
                                        </Grid>
                                        <Grid item sm={9}>
                                            <div className={classes.commentData}>
                                                <Typography
                                                    variant="h5"
                                                    component={Link}
                                                    to={`/users/${userHandle}`}
                                                    color="primary">
                                                    {userHandle}
                                                </Typography>
                                                <Typography variant="body2" color="textSecondary">
                                                    {dayjs(createdAt).format('h:mm: a, MMMM DD YYYY')}
                                                </Typography>
                                                <hr className={classes.invisibleSeparator} />
                                                <Typography variant="body1"> {body} </Typography>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                {index !== comments.length && <hr className={classes.visibleSeparator} />}
                            </Fragment>
                        )
                    })
                }
            </Grid>
        )
    }
}

Comments.propTypes = {
    comments: PropTypes.array.isRequired
};

export default withStyles(styles)(Comments);
