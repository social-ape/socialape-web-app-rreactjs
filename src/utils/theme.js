export default {
    palette: {
        primary: {
            light: '#6200ea',
            main: '#673ab7',
            dark: '#311b92',
            contrastText: '#fff'
        },
        secondary: {
            light: '#304ffe',
            main: '#f05',
            dark: '#1a237e',
            contrastText: '#fff'
        }
    },
    typography: {
        useNextVariants: true
    },
    paper: {
        padding: 20
    },
    profile: {
        '& .image-wrapper': {
            textAlign: 'center',
            position: 'relative',
            '& button': {
                position: 'absolute',
                top: '80%',
                left: '70%'
            }
        },
        '& .profile-image': {
            width: 200,
            height: 200,
            objectFit: 'cover',
            maxWidth: '100%',
            borderRadius: '50%'
        },
        '& .profile-details': {
            textAlign: 'center',
            '& span, svg': {
                verticalAlign: 'middle'
            },
            '& a': {
                color: '#311b92'
            }
        },
        '& hr': {
            border: 'none',
            margin: '0 0 10px 0'
        },
        '& svg.button': {
            '&:hover': {
                cursor: 'pointer'
            }
        }
    },
    buttons: {
        textAlign: 'center',
        '& a': {
            margin: '20px 10px'
        }
    },
    card: {
        position: 'relative',
        display: 'flex',
        marginBottom: 15
    },
    image: {
        width: 200,
        height: 170,
        objectFit: 'cover'
    },
    cover: {
        padding: 15,
        objectFit: 'cover',
        minWidth: '70%'
    },
    handle: {
        height: 25,
        width: 60,
        backgroundColor: '#673ab7',
        marginBottom: 7
    },
    date: {
        height: 15,
        width: 100,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        marginBottom: 10
    },
    fullLine: {
        height: 15,
        width: '90%',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        marginBottom: 10
    },
    haflLine: {
        height: 15,
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        marginBottom: 10
    }
}