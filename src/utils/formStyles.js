export default {
    form: {
        textAlign: 'center'
    },
    image: {
        margin: '15px auto 15px auto',
        maxWidth: 70,
        maxHeight: 70
    },
    pageTitle: {
        // margin: '15px auto 15px auto'
    },
    textField: {
        margin: '15px auto 15px auto'
    },
    button: {
        margin: '15px auto 15px auto',
        position: 'relative'
    },
    customError: {
        margin: '10px auto 10px auto',
        color: 'red'
    },
    progress: {
        position: 'absolute'
    }
}