import React from 'react';
import PropTypes from 'prop-types';

// MUI stuff
import withStyles from '@material-ui/core/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

import theme from '../utils/theme';
// import NoImg from '../images/icon.png';
import NoImg from '../images/no-img.png';

const styles = { ...theme };

const ScreamSkeleton = props => {

    const classes = props.classes;

    const content = Array.from({ length: 5 }).map((item, index) => (
        <Card className={classes.card} key={index}>
            <CardMedia className={classes.image} image={NoImg} />
            <div className="shineCover">
                <CardContent className={classes.cover}>
                    <div className={classes.handle} />
                    <div className={classes.date} />
                    <div className={classes.fullLine} />
                    <div className={classes.fullLine} />
                    <div className={classes.haflLine} />
                </CardContent>
            </div>
        </Card>
    ));

    return <>{content}</>;
}

ScreamSkeleton.propTypes = { classes: PropTypes.object.isRequired };

export default withStyles(styles)(ScreamSkeleton);
